const inquirer = require('inquirer');
var exec = require('child_process').exec, child;
const args = require('minimist')(process.argv.slice(2));

if (process.argv.length<4){
inquirer
  .prompt([
    {
      type: 'list',
      name: 'option',
      message: 'Choose an option:',
      pageSize: '16',
      choices: ['From blockchain metadata - Print blockchain metadata'
      , 'From blockchain metadata - Get Chain state'
      , 'From blockchain metadata - Call Extrinsic operation'
      , 'From provided string - Get Chain state'
      , 'From provided string - Call Extrinsic operation'],
    },
  ])
  .then(answers => {
    if (answers.option == "From blockchain metadata - Print blockchain metadata"){
      GetInfoAboutBlockchain();
    }
    if (answers.option == "From blockchain metadata - Get Chain state"){
      GetInfoAboutBlockchainFromMetadata();
    }
    if (answers.option == "From blockchain metadata - Call Extrinsic operation"){
      GetInfoIntoBlockchainFromMetadata();
    }
    if (process.argv.length==2){
      if (answers.option == "From provided string - Get Chain state"){
        // const args = require('minimist')(process.argv.slice(2));
        var arg1var;

        var questions1 = [{
            type: 'input',
            name: 'arg1',
            message: "Make a call from provided string:",
        }]

        inquirer.prompt(questions1).then(answers => {
            arg1var= (`${answers['arg1']}`);

            (async () => {
              var result = await readSpecificJob(arg1var)
              console.log('return ' + result)
              while(result != "null"){
                process.exit()
              }
            })()
        })
      }
      if (answers.option == "From provided string - Call Extrinsic operation"){
      // const args = require('minimist')(process.argv.slice(2));

        var arg1var;

        var questions1 = [{
            type: 'input',
            name: 'arg1',
            message: "Make a call from provided string:",
        }]
          
        inquirer.prompt(questions1).then(answers => {
            arg1var= (`${answers['arg1']}`);

            var StringNameOfFunction = arg1var;

            (async () => {
              var result = await makeCustomCallFromString(StringNameOfFunction)
              console.log('return ' + result)
              while(result != "null"){
                process.exit()
              }
            })()
        })
      }
    }
    if (process.argv.length>2){
      if (answers.option == "From provided string - Get Chain state"){

        var ArgumentStringTwo = JSON.stringify(args).slice(6,JSON.stringify(args).length-2);
        // ArgumentStringTwo = ArgumentStringTwo.substring(1, ArgumentStringTwo.length-1);
        console.log('ArgumentStringTwo: ' + ArgumentStringTwo);
        var ArgumentStringThree = ArgumentStringTwo.split("\",\"").join(' ')
        .slice(1,ArgumentStringTwo.split("\",\"").join(' ').length-1);
        console.log('ArgumentStringThree: ' + ArgumentStringThree);
        (async () => {
          var result = await makeCustomCallFromStringForCallingState(ArgumentStringThree)
          console.log('return ' + result)
          while(result != "null"){
            process.exit()
          }
        })()
      }
      if (answers.option == "From provided string - Call Extrinsic operation"){
        var ArgumentStringTwo = JSON.stringify(args).slice(6,JSON.stringify(args).length-2);
        console.log('ArgumentStringTwo: ' + ArgumentStringTwo);
        var ArgumentStringThree = ArgumentStringTwo.split("\",\"").join(' ').slice(1,ArgumentStringTwo.split("\",\"").join(' ').length-1);;
        console.log('ArgumentStringThree: ' + ArgumentStringThree);
        (async () => {
          var result = await makeCustomCallFromString(ArgumentStringThree)
          console.log('return ' + result)
          while(result != "null"){
            process.exit()
          }
        })()
      }
    }
});
}

function stringFromUTF8Array(data)
{
  const extraByteMap = [ 1, 1, 1, 1, 2, 2, 3, 0 ];
  var count = data.length;
  var str = "";
  
  for (var index = 0;index < count;)
  {
    var ch = data[index++];
    if (ch & 0x80)
    {
      var extra = extraByteMap[(ch >> 3) & 0x07];
      if (!(ch & 0x40) || !extra || ((index + extra) > count))
        return null;
      
      ch = ch & (0x3F >> extra);
      for (;extra > 0;extra -= 1)
      {
        var chx = data[index++];
        if ((chx & 0xC0) != 0x80)
          return null;
        
        ch = (ch << 6) | (chx & 0x3F);
      }
    }
    
    str += String.fromCharCode(ch);
  }
  
  return str;
}

function lowercaseFirstLetter(string) {
  return string.charAt(0).toLowerCase() + string.slice(1);
}

function  GetInfoAboutBlockchain(){
  const { ApiPromise, WsProvider } = require('@polkadot/api');

  async function main() {
  
    // const provider = new WsProvider('ws://127.0.0.1:9944');
    // const api = await ApiPromise.create({ provider });
    const api = await ApiPromise.create();
  
  const meta = await api.rpc.state.getMetadata();
  // console.log(JSON.stringify(meta.metadata));
  // console.log("--------------------PAUSE--------------------");

  const ALICE = '5GrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY';

  const [chain, nodeName, nodeVersion] = await Promise.all([
      api.rpc.system.chain(),
      api.rpc.system.name(),
      api.rpc.system.version()
    ]);

    console.log(`chain ` + chain);
    console.log(`nodeName ` + nodeName);
    console.log(`nodeVersion ` + nodeVersion);
  
  const [now, validators] = await Promise.all([
    api.query.timestamp.now()
  ]);
  
  console.log('The current date is: ' + now);

  // Retrieve the chain & node information information via rpc calls
  const [properties] = await Promise.all([
    api.rpc.system.properties()
  ]);
  console.log('You are connected to chain ' + chain)
  console.log('You are using: ' + nodeName + ' v' + nodeVersion);

  if (properties.size > 0) {
    console.log('Node specific properties:');
    properties.forEach((value, key) => {
      console.log(key, value);
    });
  } else {
    console.log('No specific chain properties found.');
  }

  const currHash = await api.query.balances.freeBalance.hash(ALICE);
  const currSize = await api.query.balances.freeBalance.size(ALICE);

  console.log('Alice balance entry has a value hash of', currHash, 'with a size of', currSize);


  // retrieve the balance, once-off at the latest block
  const currBalance = await api.query.balances.freeBalance(ALICE);

  console.log('Alice has a current balance of', currBalance);

  // retrieve balance updates with an optional value callback
  const balanceUnsub = await api.query.balances.freeBalance(ALICE, (balance) => {
    console.log('Alice has an updated balance of', balance);
  });

  const { magicNumber,metadata } = await api.rpc.state.getMetadata();

  console.log( 'Magic number: ' + magicNumber );
  console.log( 'Metadata: ' + metadata.raw );

  }
  main().catch(console.error).finally(() => process.exit());
}

function  GetInfoAboutBlockchainFromMetadata(){
  const { ApiPromise, WsProvider } = require('@polkadot/api');
  const TypeArrayU64 = [];
  const ModulesNames = [];

  var indexCounterForInquirer = new Object();
  var TypeArrayU64Dictionary = new Object();
  
  async function main() {
  
  const api = await ApiPromise.create();
  const meta = await api.rpc.state.getMetadata();
  const ALICE = '5GrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY';
  const { magicNumber,metadata } = await api.rpc.state.getMetadata();
  var myObj = JSON.parse(metadata.raw);

  // console.log( 'myObj.modules.length: ' + myObj.modules.length );

  for (i = 0; i < myObj.modules.length; i++) {
    ModulesNames.push(myObj.modules[i].name);
    indexCounterForInquirer[myObj.modules[i].name]=i;
  }

  inquirer
    .prompt([
      {
        type: 'list',
        name: 'option',
        message: 'Choose an option:',
        pageSize: '20',
        choices: ModulesNames,
      },
    ])
    .then(answers => {
      var ModulesAnswersString = answers.option;
      // console.log('answers ' + ModulesAnswersString);

      var indexAnswers = indexCounterForInquirer[ModulesAnswersString];
      // console.log(indexAnswers);
      
      for (ii = 0; ii < myObj.modules[indexAnswers].storage.length; ii++) {
        // console.log( 'myObj: ' + myObj.modules[indexAnswers].storage[ii].name );
        // console.log( 'String: ' + myObj.modules[indexAnswers].name
        // + "." + myObj.modules[indexAnswers].storage[ii].name );
        // console.log( 'Type: ' + myObj.modules[indexAnswers].storage[ii].type.Type );
        

        if(myObj.modules[indexAnswers].storage[ii].type.Map != undefined){
          console.log(myObj.modules[indexAnswers].storage[ii].type.Map);
          console.log(myObj.modules[indexAnswers].storage[ii].type.Map['key']);

          TypeArrayU64.push((myObj.modules[indexAnswers].name + "." + myObj.modules[indexAnswers].storage[ii].name).toString());
          TypeArrayU64Dictionary[TypeArrayU64[ii]]=myObj.modules[indexAnswers].storage[ii].type.Map['key'];
        }
        if(myObj.modules[indexAnswers].storage[ii].type.Map == undefined){
          TypeArrayU64.push((myObj.modules[indexAnswers].name + "." + myObj.modules[indexAnswers].storage[ii].name).toString());
          TypeArrayU64Dictionary[TypeArrayU64[ii]]=myObj.modules[indexAnswers].storage[ii].type.Type;
        }
        
      }
      
      // console.log('!!!!!!! ');
      // console.log(TypeArrayU64);
      // console.log(TypeArrayU64Dictionary);

      inquirer
        .prompt([
          {
            type: 'list',
            name: 'option',
            message: 'Choose an option:',
            pageSize: '10',
            choices: TypeArrayU64,
          },
        ])
        .then(answers => {

            var answersString = answers.option;
            console.log('answers ' + answersString);
            console.log('answers ' + TypeArrayU64Dictionary[answersString]);

            var answersString1 = answersString.replace('SubstrateTestModule', 'substrateTestModule');
            answersString1 = answersString1.replace('JobCoordinator', 'jobCoordinator');
            var res = answersString1.split(".");
            answersString1 = res[0] + "."  + lowercaseFirstLetter(res[1]);



            if(TypeArrayU64Dictionary[answersString] != 'u64' || TypeArrayU64Dictionary[answersString] != 'Bytes'){
              console.log('ALICE - ' + '5GrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY');
              var arg2var;

              var questions2 = [{
                  type: 'input',
                  name: 'arg2',
                  message: "Input " + TypeArrayU64Dictionary[answersString] + " : ",
              }]
      
              inquirer.prompt(questions2).then(answers => {
                  arg2var= (`${answers['arg2']}`);

                  var functionString = makeStringIntoFunctionWithArg(answersString1, answers['arg2']);
                  console.log('functionString ' + functionString);
      
                  (async () => {
                    var result = await readSpecificJob(functionString)
                    console.log('return ' + result)
                    while(result != "null"){
                      process.exit()
                    }
                  })()

              })
            }
            if(TypeArrayU64Dictionary[answersString] == 'u64' || TypeArrayU64Dictionary[answersString] == 'Bytes'){

              var functionString = makeStringIntoFunction(answersString1);
              console.log('functionString ' + functionString);
  
              (async () => {
                var result = await readSpecificJob(functionString)
                console.log('return ' + result)
                while(result != "null"){
                  process.exit()
                }
              })()

            }


        });

    });

  }
  
  main().catch(console.error);
}

function makeStringIntoFunction(String)
{
  var functionString = 'api.query.' + String + '()';
  return functionString;
}

function makeStringIntoFunctionWithArg(String, Arg)
{
  var functionString = 'api.query.' + String + '(\'' + Arg + '\')';
  return functionString;
}

async function readSpecificJob(StringNameOfFunction)
{
var stringToReturn = null;
async function getTsReadSpecificJob() {
console.log('StringNameOfFunction ' + StringNameOfFunction);
const { ApiPromise, WsProvider } = require('@polkadot/api');
const api = await ApiPromise.create({
  types: {
    Job: {
      ID: 'Hash',
      current_bid: 'Balance',
      weights: 'Vec<u8>', 
      trainer: 'Vec<u8>', 
      highest_bid: 'u64', 
      highest_bidder: 'u64'
    }
  }
}); 
eval('var result = ' + StringNameOfFunction); 
stringToReturn = await (result);
console.log('string2 '+ stringToReturn);
return stringToReturn;
}

async function mainReadSpecificJob() {
  const response = await getTsReadSpecificJob();
  console.log('string1 '+ response);
  console.log(response)
  return response;
}
// await main().catch(console.error).finally(() => process.exit());
return await mainReadSpecificJob().catch(console.error)
}

async function makeCustomCallFromStringForCallingState(StringNameOfFunction)
{
  var stringToReturn = null;

  const { ApiPromise, WsProvider } = require('@polkadot/api');
  const api = await ApiPromise.create({
    types: {
      Job: {
        ID: 'Hash',
        current_bid: 'Balance',
        weights: 'Vec<u8>', 
        trainer: 'Vec<u8>', 
        highest_bid: 'u64', 
        highest_bidder: 'u64'
      }
    }
  }); 

  const { Keyring } = require('@polkadot/keyring');
  'use strict';
  const BOB = '5FHneW46xGXgs5mUiveU4sbTyGBzmstUspZC92UhjJM694ty';
  const ALICE = '5GrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY';
  const keyring = new Keyring({ type: 'sr25519' });
  const aliceFromKeyring = keyring.addFromUri('//Alice');

  async function getTsReadSpecificJobStringForCallingState() {
  console.log('StringNameOfFunction ' + StringNameOfFunction);



  var result;
  console.log(StringNameOfFunction);
  eval('result = ' + StringNameOfFunction); 
  console.log(StringNameOfFunction);
  stringToReturn = await (result);
  console.log('string2 '+ stringToReturn);
  return stringToReturn;
  }
  
  async function mainReadSpecificJobStringForCallingState() {
    const response1 = await getTsReadSpecificJobStringForCallingState();
    console.log('string1 '+ response1);
    console.log(response1)
    return response1;
  }
  // await main().catch(console.error).finally(() => process.exit());
  return await mainReadSpecificJobStringForCallingState()
}

async function makeCustomCallFromString(StringNameOfFunction)
{
  var stringToReturn = null;
  async function getTsMakeCustomCallFromString() {
  console.log('StringNameOfFunction ' + StringNameOfFunction);
  const { ApiPromise, WsProvider } = require('@polkadot/api');
  const api = await ApiPromise.create({
    types: {
      Job: {
        ID: 'Hash',
        current_bid: 'Balance',
        weights: 'Vec<u8>', 
        trainer: 'Vec<u8>', 
        highest_bid: 'u64', 
        highest_bidder: 'u64'
      }
    }
  }); 

  const { Keyring } = require('@polkadot/keyring');
  'use strict';
  const BOB = '5FHneW46xGXgs5mUiveU4sbTyGBzmstUspZC92UhjJM694ty';
  const ALICE = '5GrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY';
  const keyring = new Keyring({ type: 'sr25519' });
  const aliceFromKeyring = keyring.addFromUri('//Alice');


  // const transfer = api.tx.substrateTestModule.zTestAdderToStorage(4, 4);
  
  var transfer;

  eval('transfer = ' + StringNameOfFunction); 

  const hash = await transfer.signAndSend(aliceFromKeyring);
  console.log('Transfer sent with hash', hash.toHex());


  // eval('var result = ' + StringNameOfFunction); 
  // stringToReturn = await (result);
  // console.log('string2 '+ stringToReturn);
  return hash.toHex();
  }
  
  async function mainMakeCustomCallFromString() {
    const response = await (getTsMakeCustomCallFromString());
    console.log('string1 '+ response);
    console.log(response)
    return response;
  }
  // await main().catch(console.error).finally(() => process.exit());
  return await mainMakeCustomCallFromString().catch(console.error)
}

function GetInfoIntoBlockchainFromMetadata(){
  const { ApiPromise, WsProvider } = require('@polkadot/api');
  const TypeArrayU64 = [];
  const ModulesNames = [];

  var indexCounterForInquirer = new Object();
  var TypeArrayU64DictionaryName = new Object();
  var TypeArrayU64DictionaryType = new Object();
  var TypeArrayU64DictionaryIndexer = new Object();

  var TypeArrayU64Dictionary = new Map();
  
  async function main() {
  
  const api = await ApiPromise.create();
  const meta = await api.rpc.state.getMetadata();
  const ALICE = '5GrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY';
  const { magicNumber,metadata } = await api.rpc.state.getMetadata();
  var myObj = JSON.parse(metadata.raw);

  // console.log( 'myObj.modules.length: ' + myObj.modules.length );

  for (i = 0; i < myObj.modules.length; i++) {
    ModulesNames.push(myObj.modules[i].name);
    indexCounterForInquirer[myObj.modules[i].name]=i;
  }

  // console.log( ModulesNames );

  inquirer
    .prompt([
      {
        type: 'list',
        name: 'option',
        message: 'Choose an option:',
        pageSize: '20',
        choices: ModulesNames,
      },
    ])
    .then(answers => {
      var ModulesAnswersString = answers.option;
      // console.log('answers ' + ModulesAnswersString);

      var indexAnswers = indexCounterForInquirer[ModulesAnswersString];
      // console.log(indexAnswers);
      
      for (ii = 0; ii < myObj.modules[indexAnswers].calls.length; ii++) {

        // console.log( 'String: ' + myObj.modules[indexAnswers].name + "." + myObj.modules[indexAnswers].calls[ii].name );

        TypeArrayU64.push((myObj.modules[indexAnswers].name + "." + myObj.modules[indexAnswers].calls[ii].name).toString());

        for (iii = 0; iii < myObj.modules[indexAnswers].calls[ii].args.length; iii++) {
          // console.log( 'name: ' + myObj.modules[indexAnswers].calls[ii].args[iii].name );
          // console.log( 'type: ' + myObj.modules[indexAnswers].calls[ii].args[iii].type );

          TypeArrayU64Dictionary.set((TypeArrayU64[ii]+ " " +(iii+1)).toString(), {
            keyIndex:  ii,
            keyName: TypeArrayU64[ii],
            valueName: myObj.modules[indexAnswers].calls[ii].args[iii].name,
            valueType: myObj.modules[indexAnswers].calls[ii].args[iii].type,
          });

          TypeArrayU64DictionaryIndexer[TypeArrayU64[ii]]=iii;
        }
      }
      
      // console.log('!!!!!!! ');
      // console.log(TypeArrayU64);
      // console.log(TypeArrayU64Dictionary);
      // console.log(TypeArrayU64DictionaryType);
      // console.log(TypeArrayU64DictionaryIndexer);

      inquirer
        .prompt([
          {
            type: 'list',
            name: 'option',
            message: 'Choose an option:',
            pageSize: '10',
            choices: TypeArrayU64,
          },
        ])
        .then(answers => {

            var answersString = answers.option;
            // console.log('1 ' + answersString);

            indexMeasuringHowManyQuestions = 0;
            const collectInputs = async (inputs = []) => {
              const prompts = [
                {
                  type: 'input',
                  name: TypeArrayU64Dictionary.get((answersString+ " " + (indexMeasuringHowManyQuestions+1)).toString()).valueName,
                  message: 'Enter variable with name -  \"' + TypeArrayU64Dictionary.get((answersString+ " " + (indexMeasuringHowManyQuestions+1)).toString()).valueName
                   + "\" and type - \"" + TypeArrayU64Dictionary.get((answersString+ " " + (indexMeasuringHowManyQuestions+1)).toString()).valueType + "\""
                }
              ];
            
              const { again, ...answers } = await inquirer.prompt(prompts);
              const newInputs = [...inputs, answers];

              while(indexMeasuringHowManyQuestions<TypeArrayU64DictionaryIndexer[answersString])
              {
                indexMeasuringHowManyQuestions++;
                return collectInputs(newInputs);
              }

              return newInputs;
            };
            
            const getInputFunctionMain = async () => {
              const inputs = await collectInputs();
              // console.log(inputs);
              var endString = 'api.tx.' + answersString + '(';

              // console.log(JSON.stringify(inputs[0]).split(":")[1].substring(1, JSON.stringify(inputs[0]).split(":")[1].length-2));

              for (iiii = 0; iiii < inputs.length; iiii++) {
                endString = endString + JSON.stringify(inputs[iiii]).split(":")[1].substring(1, JSON.stringify(inputs[iiii]).split(":")[1].length-2) + ','
              };

              endString = endString.substring(0, endString.length - 1); 
              endString += ')';
              endString = endString.replace('SubstrateTestModule', 'substrateTestModule');
              endString = endString.replace('JobCoordinator', 'jobCoordinator');

              // console.log("endString " + endString);
              var camelCasedString = camelCase(endString);

              // console.log("camelCased " + camelCasedString);

              // var StringNameOfFunction = 'api.tx.substrateTestModule.zTestAdderToStorage(' + arg1var + ',' + arg2var +')';

              (async () => {
                var result = await makeCustomCallFromString(camelCasedString)
                console.log('return ' + result)
                while(result != "null"){
                  process.exit()
                }
              })()

            };
            
            getInputFunctionMain();

        });
    });

  }
  main().catch(console.error);
}

var camelCase = (function () {
  var DEFAULT_REGEX = /[-_]+(.)?/g;

  function toUpper(match, group1) {
      return group1 ? group1.toUpperCase() : '';
  }

  return function (str, delimiters) {
      return str.replace(delimiters ? new RegExp('[' + delimiters + ']+(.)', 'g') : DEFAULT_REGEX, toUpper);
  };
})();

if (process.argv.length==4){
  var ArgumentStringTwo = JSON.stringify(process.argv);
  console.log('ArgumentStringTwo: ' + ArgumentStringTwo);

  if (process.argv[2]=="-state" || process.argv[2]=="-s"){
    (async () => {
      var result = await makeCustomCallFromStringForCallingState(process.argv[3])
      console.log('return ' + result)
      while(result != "null"){
        process.exit()
      }
    })()
  }
  else if (process.argv[2]=="-operation" || process.argv[2]=="-o"){
    (async () => {
      var result = await makeCustomCallFromString(process.argv[3])
      console.log('return ' + result)
      while(result != "null"){
        process.exit()
      }
    })()
  }
  else {
    console.log('No valid option selected. "-state" and "-operation" are only excepted');
  }
}

if (process.argv.length>4){
  console.log('Too many arguments!');
  console.log('    -2 arguments for calling the interface with a menu');
  console.log('    -3 arguments for calling the interface with a menu and directly calling state/operations');
  console.log('    -4 arguments for calling the interface with a predefined function');
}