SubstrateLocation="${PWD%/[^/]*}/SubstrateJobCoordinator/SubstrateJobCoordinator/" 

cd $SubstrateLocation
./scripts/init.sh
./scripts/build.sh
cargo build --release
