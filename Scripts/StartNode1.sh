SubstrateLocation="${PWD%/[^/]*}/SubstrateJobCoordinator/SubstrateJobCoordinator/target/release/substratekitties"
BlockchainLocationDev="${PWD%/[^/]*}/Blockchain/dev" 

rm -rf $BlockchainLocationDev  
$SubstrateLocation --base-path $BlockchainLocationDev --chain=local --key //Dev --port 30333 --telemetry-url ws://telemetry.polkadot.io:1024 --validator --name DevNode  
