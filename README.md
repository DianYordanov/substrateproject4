#Quick tutorial:
--------------------------------------------------------------------------------
## Requirements:

- Linux: Ubuntu-based distributions: Ubuntu, Mint, Pop!_OS
- Rust: if you do not have it installed run:
>curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
- Substrate: to install:
>curl https://getsubstrate.io -sSf | bash -s -- --fast
- Node + NPM ( download and install from https://nodejs.org/en/download/ )
- inquirer: used in the js CLI GUI, install with:
>npm i inquirer
- Yarn - for full web GUI ( download and install from https://yarnpkg.com/lang/en/ )
- Firefox - for full web GUI (usually run on http://localhost:3000 and is the one we will use here)
- Chrome - for minimal web GUI (usually run on http://localhost:8000 )
  
## Operating Substrate with the help of shell scripts:

There are 5 scripts in the "Scripts" folder. These scripts are used to build, start the GUI and start the different nodes that operate on Substrate. 

### Build.sh 

Build.sh is a build script that will check if Substrate is the latest version, update it if it is not and then try to build a new webassembly binary in the target directory of SubstrateJobCoordinator/SubstrateJobCoordinator

After making any changes to SubstrateJobCoordinator/SubstrateJobCoordinator/runtime/src run ./Build.sh 

### RunGUI.sh

RunGUI.sh runs the full Substrate GUI located in SubstrateJobCoordinator/UI

### StartNode1.sh / StartNode2.sh / StartNode3.sh

Each of these scripts run a different Substrate node. The first node is required and all other nodes are attached to it. All nodes have to be executed in different terminal tabs/windows/clients. 

## General information about the Substrate modules and rust code

The Substrate Rust modules that we will be working with are all located in SubstrateJobCoordinator/SubstrateJobCoordinator/runtime/src . There are currently 3 Rust modules there, but more can be added in the future or by the user. 

### lib.rs

lib.rs is the main Rust module for the project. It is used to control which modules are called and the settings/specifications of the project. When adding a module you have to add it by adding lines of code in 3 places of this module: 

1. in the mod section - for example: 
>"mod JobCoordinator;" 
2. in the "iml" section where you also have to implement the type of runtime traits, such as events - for example: 

>impl JobCoordinator::Trait for Runtime {
    type Event = Event;
}
3. in the construct_runtime section where you have to specify module name and all traits - for example:
>JobCoordinatorModule: JobCoordinator::{Module, Call, Storage, Event<T>},

### JobCoordinator.rs

JobCoordinator.rs is the full module for coordinating jobs on the Substrate blockchain. It implements events and modules for creating and transmitting jobs as well as a configuration for jobs ( "pub struct Job<Hash, Balance>" ). The full job sctruct configuration is:

>pub struct Job<Hash, Balance> { 
    ID: Hash, 
    current_bid: Balance, 
    weights: Vec<u8>, 
    trainer: Vec<u8>, 
    highest_bid: u64, 
    highest_bidder: u64, 
} 

The following description of JobCoordinator.js may change with the time, but for now it is as following:

The storage is divided into a section for primitive variables and Job-related variables that rely on the struct configuration and Substrate native functions. The decl_module contains the modules used in Substrate and contains functions for creating, selling, buying and transferring jobs. These functions use the 2 event modules: "mint" and "transfer_from" when they need to write or change job states into the chain state storage. 

### SubstrateTestModule.rs

SubstrateTestModule.rs is a test module added to test Substrate functionality. It is as simple as possible has only 3 modules, 2 events, and 3 storage variables, without including structs. The modules included here are "z_test_set_bool_value", "z_test_adder_to_storage" and "z_test_bytes_adder". These modules test changing chain state, adding integers using events and printing output to the terminal.

## Node.js CLI interface for Substrate:

An interface for Substrate written in Node.js using inquirer is located in SubstrateCLI. The interface is implemented using https://polkadot.js.org/api/start/ . The interface has 2 main goals: to be able to substitute the web GUI and to be able to call specific chain states and extrinsic operations directly from the command line. The interface unlocks a couple of important functionalities that the web interface cannot do:

- Ability to connect with Linux command line utilities and shell scripting.
- Ability to interact with the blockchain without the use of a browser interface thus making it possible to use other programming languages and shell scripts to interact with Substrate or even use SSH for remote connection to the blockchain
- Ability to use Substrate in a big data management software like Spark clusters. 
- Use of fewer resources: a command line utility vs a React based web interface. 

The interface can use more of the polkadot-js/api utilities but for now, it is intended to only be used for getting chain state and calling extrinsic operations. This means that in a normal development use of Substrate, developers would still be required to use the web GUI to setup settings and do operations other than getting chain state and calling extrinsic operations. 

The interface still requires the use of Substrate blockchain nodes running - at least 2 nodes have to have been started using the shell scripts provided in the "Scripts" directory.

![](Images/Interface.png)

### Working with the interface:

To use the interface, go to the SubstrateCLI directory and type:
>node Interface.js

Assuming you have the Node.js package "inquirer" installed, you should see a menu with options divided into 2 main categories: 
-Directly working with the Substrate interface by choosing commands from the CLI interface
-Directly calling commands by a provided string. 

### Custom command strings

One of the main uses of the interface is to use custom command strings. These can either be called by the options named: "From provided string - Get Chain state" and "From provided string - Call Extrinsic operation" or as command line arguments when calling the Node.js interface. In order to get the strings, you can either execute interface menu commands and get the code for their execution when the commands get executed, or you can construct the string yourself. The command will be outputted in the terminal under the name of "StringNameOfFunction". The command strings have the following format:

>api.query.substrateTestModule.test_my_storage()

for getting chain state and:

>api.tx.substrateTestModule.zTestAdderToStorage(3,4)

for calling extrinsic operations. 

If you want to do the call using command line arguments, you can do it by calling it in the following way:

>node Interface.js "api.tx.substrateTestModule.zTestAdderToStorage(4,4)"

and using the "From provided string - Get Chain state" or "From provided string - Call Extrinsic operation" interface options. 

### Direct state/operation callings:

You can skip using the menu and call extrinsic operations and get chain states directly. To do this have 4 arguments when calling the program: "node", "Interface.js", type of command and command string. The type of command can be "-state" or "-s" for getting chain state or "-operation" / "-o" for calling extrinsic operation. The command string has the same syntax as discussed in the "Custom command strings" section. The following are examples of valid command line interface executions using only arguments:

>node Interface.js -o "api.tx.substrateTestModule.zTestAdderToStorage(3,4)"

for calling an extrinsic operation "zTestAdderToStorage(3,4)", and

>node Interface.js -s "api.query.substrateTestModule.test_my_storage()"

for getting a chain state of "test_my_storage()". 

## Troubleshooting of CLI interface:

### "API-WS: disconnected" mistake:

>2019-10-23 06:33:41          API-WS: disconnected from ws://127.0.0.1:9944 code: '1006' reason: 'connection failed'
2019-10-23 06:33:42          API-WS: disconnected from ws://127.0.0.1:9944 code: '1006' reason: 'connection failed'

You need to have the Substrate blockchain running in the background on the host/Clients. If no Substrate blockchain nodes are running, you will get the above mistake. 

### non-updating values in blockchain state:

It may take some time ( up to 20seconds ) for blockchain block update and thus update of the blockchain state after an operation has been submitted. 

### under_scores, camelCase and PascalCase:

This is usually not a problem if naming conventions are followed but can lead to a problem: Rust ( Substrate modules ) are using the under_scores naming convention and converting it into the web GUI interface. You will get problems with the web interface if you do not follow the naming convention and thus it is assumed that you will use it for the CLI interface as well. The interface being written in Javascript however uses camelCase naming convention and has to emulate the naming convention translation of the web interface. This means that even though you may get an under_score list of blockchain chain states or extrinsic operations they will be translated into camelCase. All this means is that if you follow naming conventions of the different languages you will get no problems, however, if you do not, you may encourage problems with both the web and the CLI interfaces. 

## Troubleshooting of web GUI:

### Error 1002:

![](Images/1002Error.png)

Fixed by refreshing the web GUI. May need a couple of refreshes.

### "\<unknown\>" Chain state:

![](Images/UNKNOWNerror.png)

A couple of possible problems:

1. Broken blockchain config either in Settings -> Developer section of the full web GUI
2. Broken blockchain config in code
3. May happen if you use Chrome 

Fixed by making sure the blockchain config is right and that you use Firefox for the web GUI

### Problems with shell scripts:

" Scripts/StartNode1.sh ; Scripts/StartNode2.sh ; Scripts/StartNode3.sh " are using " rm -rf $BlockchainLocationDev " to delete old blockchain and create a new one. If these scrips fail, run them with " sudo ". Another alternative is to use the Substrate innate function " ./target/release/substratekitties purge-chain --dev " but " rm -rf " has been found to work better. 

#Miscellaneous
--------------------------------------------------------------------------------

##Struct config:

This is the Struct config for the jobs located in json format in the Settings -> Developer section of the full web GUI

```
{
  "Job": {
    "ID": "Hash",
    "current_bid": "Balance",
    "weights": "Vec<u8>",
    "trainer": "Vec<u8>",
    "highest_bid": "u64",
    "highest_bidder": "u64"
  }
}
```
##easyGit.sh:

This is a small script for easy committing to git and is not connected to Substrate