use support::{decl_storage, decl_module, StorageValue, StorageMap,
    dispatch::Result, ensure, decl_event, traits::Currency};
use system::ensure_signed; 
use runtime_primitives::traits::{As, Hash, Zero}; 
use parity_codec::{Encode, Decode}; 
use rstd::cmp; 
use rstd::prelude::Vec; 

pub trait Trait: balances::Trait { 
    type Event: From<Event<Self>> + Into<<Self as system::Trait>::Event>; 
} 

decl_event!( 
    pub enum Event<T> 
    where 
        <T as system::Trait>::AccountId, 
        <T as system::Trait>::Hash, 
        <T as balances::Trait>::Balance 
    { 
        Created(AccountId, Hash), 
        PriceSet(AccountId, Hash, Balance), 
        Transferred(AccountId, AccountId, Hash), 
        Bought(AccountId, AccountId, Hash, Balance), 
    } 
); 

decl_storage! { 
    trait Store for Module<T: Trait> as KittyStorage { 

        Nonce: u64;
        test_my_storage: u64;
        test_byte_variable: Vec<u8>; 

    } 
} 

decl_module! { 
    pub struct Module<T: Trait> for enum Call where origin: T::Origin { 
        fn deposit_event<T>() = default; 

        fn z_test_set_bool_value(origin, value1: u64) -> Result { 
          let sender = ensure_signed(origin)?; 
            #[test] 
                pub fn test() { 
                    println!("format {} arguments", "some"); 
                } 
            Ok(()) 
        } 
        fn z_test_adder_to_storage(origin, num1: u64, num2: u64) -> Result { 
                let _sender = ensure_signed(origin)?; 
                let result = Self::_test_adder(num1, num2); 
                Self::_test_store_value(result)?; 
                Ok(()) 
        } 
        fn z_test_bytes_adder(origin, str3: Vec<u8>)-> Result { 
                let _sender = ensure_signed(origin)?; 
                <test_byte_variable<T>>::put(str3); 
                runtime_io::print("leftover share wasted due to hash collision");  
                Ok(()) 
        } 
    } 
} 

impl<T: Trait> Module<T> {
    fn _test_adder(num1: u64, num2: u64) -> u64 { 
        let final_answer: u64 = num1 + num2; 
    return final_answer; 
    } 
    fn _test_store_value(value: u64) -> Result { 
        <test_my_storage<T>>::put(value); 
        Ok(()) 
    } 
}
