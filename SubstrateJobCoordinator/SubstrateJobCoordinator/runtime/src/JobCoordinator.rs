use support::{decl_storage, decl_module, StorageValue, StorageMap,
    dispatch::Result, ensure, decl_event, traits::Currency};
use system::ensure_signed; 
use runtime_primitives::traits::{As, Hash, Zero}; 
use parity_codec::{Encode, Decode}; 
use rstd::cmp; 
use rstd::prelude::Vec; 

#[derive(Encode, Decode, Default, Clone, PartialEq)] 
#[cfg_attr(feature = "std", derive(Debug))]
//Struct config, any changes here have to be repeated in json format in the Settings -> Developer section
//Do not use other types of primitives than 'Vec<u8>' for strings. 
pub struct Job<Hash, Balance> { 

    ID: Hash, 
    current_bid: Balance, 
    weights: Vec<u8>, 
    trainer: Vec<u8>, 
    highest_bid: u64, 
    highest_bidder: u64, 

} 

pub trait Trait: balances::Trait { 
    type Event: From<Event<Self>> + Into<<Self as system::Trait>::Event>; 
} 

decl_event!( 
    pub enum Event<T> 
    where 
        <T as system::Trait>::AccountId, 
        <T as system::Trait>::Hash, 
        <T as balances::Trait>::Balance 
    { 
        Created(AccountId, Hash), 
        PriceSet(AccountId, Hash, Balance), 
        Transferred(AccountId, AccountId, Hash), 
        Bought(AccountId, AccountId, Hash, Balance), 
    } 
); 

decl_storage! { 
    trait Store for Module<T: Trait> as Storage { 

        OwnedJob get(job_of_owner): map T::AccountId => Job<T::Hash, T::Balance>; 
        Jobs get(Job): map T::Hash => Job<T::Hash, T::Balance>;
        AllJobsArray get(job_by_index): map u64 => T::Hash;
        AllJobsCount get(all_kitties_count): u64;
        AllJobsIndex: map T::Hash => u64;
        JobOwner get(owner_of): map T::Hash => Option<T::AccountId>;
        OwnedJobsArray get(job_of_owner_by_index): map (T::AccountId, u64) => T::Hash;
        OwnedJobsCount get(owned_job_count): map T::AccountId => u64;
        OwnedJobsIndex: map T::Hash => u64;

        _primitive_ID_storage: map T::AccountId => u64; 
        _primitive_weights_storage: Vec<u8>; 
        _primitive_trainer_storage: Vec<u8>; 
        _primitive_highest_bid_storage: u64; 
        _primitive_highest_bidder_storage: u64; 
        _primitive_Nonce: u64;

    } 
} 

decl_module! { 
    pub struct Module<T: Trait> for enum Call where origin: T::Origin { 
        fn deposit_event<T>() = default; 
        fn submit_job(origin, ID_var: u64, current_bid_var: T::Balance, weights_var: Vec<u8>, trainer_var: Vec<u8>, highest_bid_var: u64, highest_bidder_var: u64,  nonce: u64) -> Result { 
                let sender = ensure_signed(origin)?; 

                <_primitive_ID_storage<T>>::insert(&sender,ID_var); 
                <_primitive_weights_storage<T>>::put(&weights_var); 
                <_primitive_trainer_storage<T>>::put(&trainer_var); 
                <_primitive_highest_bid_storage<T>>::put(highest_bid_var); 
                <_primitive_highest_bidder_storage<T>>::put(highest_bidder_var); 

                let random_hash_data = (<system::Module<T>>::random_seed(), &sender, &nonce).using_encoded(<T as system::Trait>::Hashing::hash);
                ensure!(!<JobOwner<T>>::exists(random_hash_data), "Job already exists");

                let new_job = Job { 
                ID: random_hash_data,
                current_bid: current_bid_var, 
                weights: weights_var, 
                trainer: trainer_var, 
                highest_bid: highest_bid_var, 
                highest_bidder: highest_bidder_var, 
                }; 

                <OwnedJob<T>>::insert(&sender, new_job); 
                Ok(()) 
        } 
        fn submit_test_job(origin, current_bid_var: T::Balance) -> Result { 
                let sender = ensure_signed(origin)?; 

                <_primitive_ID_storage<T>>::insert(&sender,0); 
                <_primitive_weights_storage<T>>::put([0x11].to_vec()); 
                <_primitive_trainer_storage<T>>::put([0x12].to_vec()); 
                <_primitive_highest_bid_storage<T>>::put(2); 
                <_primitive_highest_bidder_storage<T>>::put(3); 

                let new_job = Job { 
                ID: <T as system::Trait>::Hashing::hash_of(&0), 
                current_bid: current_bid_var, 
                weights: [0x11].to_vec(), 
                trainer: [0x12].to_vec(), 
                highest_bid: 2, 
                highest_bidder: 3, 
                }; 

                <OwnedJob<T>>::insert(&sender, new_job); 
                Ok(()) 
        } 

        fn create_and_mint_job(origin, current_bid_var: T::Balance) -> Result {
            let sender = ensure_signed(origin)?;
            let nonce = <_primitive_Nonce<T>>::get();
            let random_hash1 = (<system::Module<T>>::random_seed(), &sender, nonce)
                .using_encoded(<T as system::Trait>::Hashing::hash);

            let new_job = Job { 
                ID: random_hash1, 
                current_bid: current_bid_var, 
                weights: [0x11].to_vec(), 
                trainer: [0x12].to_vec(), 
                highest_bid: 2, 
                highest_bidder: 3, 
            }; 

            Self::mint(sender, random_hash1, new_job)?;

            <_primitive_Nonce<T>>::mutate(|n| *n += 1);

            Ok(())
        }

        fn set_current_bid(origin, job_id: T::Hash, new_bid: T::Balance) -> Result {
            let sender = ensure_signed(origin)?;

            ensure!(<Jobs<T>>::exists(job_id), "This cat does not exist");

            let owner = Self::owner_of(job_id).ok_or("No owner for this Job")?;
            ensure!(owner == sender, "You do not own this cat");

            let mut Job = Self::Job(job_id);
            Job.current_bid = new_bid;

            <Jobs<T>>::insert(job_id, Job);

            Self::deposit_event(RawEvent::PriceSet(sender, job_id, new_bid));

            Ok(())
        }

        fn transfer_job(origin, to: T::AccountId, job_id: T::Hash) -> Result {
            let sender = ensure_signed(origin)?;

            runtime_io::print("doing a transfer");  

            let owner = Self::owner_of(job_id).ok_or("No owner for this Job")?;
            ensure!(owner == sender, "You do not own this Job");

            Self::transfer_from(sender, to, job_id)?;

            Ok(())
        }

        fn buy_job(origin, job_id: T::Hash, max_bid: T::Balance) -> Result {
            let sender = ensure_signed(origin)?;

            ensure!(<Jobs<T>>::exists(job_id), "This cat does not exist");

            let owner = Self::owner_of(job_id).ok_or("No owner for this Job")?;
            ensure!(owner != sender, "You can't buy your own cat");

            let mut Job = Self::Job(job_id);

            let job_bid = Job.current_bid;
            ensure!(!job_bid.is_zero(), "The cat you want to buy is not for sale");
            ensure!(job_bid <= max_bid, "The cat you want to buy costs more than your max bid");

            <balances::Module<T> as Currency<_>>::transfer(&sender, &owner, job_bid)?;

            Self::transfer_from(owner.clone(), sender.clone(), job_id)
                .expect("`owner` is shown to own the Job; \
                `owner` must have greater than 0 kitties, so transfer cannot cause underflow; \
                `all_job_count` shares the same type as `owned_job_count` \
                and minting ensure there won't ever be more than `max()` kitties, \
                which means transfer cannot cause an overflow; \
                qed");

            Job.current_bid = <T::Balance as As<u64>>::sa(0);
            <Jobs<T>>::insert(job_id, Job);

            Self::deposit_event(RawEvent::Bought(sender, owner, job_id, job_bid));

            Ok(())
        }

    } 
} 

impl<T: Trait> Module<T> {
    //used for creating job tokens
    fn mint(to: T::AccountId, job_id: T::Hash, new_job: Job<T::Hash, T::Balance>) -> Result {
        ensure!(!<JobOwner<T>>::exists(job_id), "Job already exists");

        let owned_job_count = Self::owned_job_count(&to);

        let new_owned_job_count = owned_job_count.checked_add(1)
            .ok_or("Overflow adding a new Job to account balance")?;

        let all_kitties_count = Self::all_kitties_count();

        let new_all_kitties_count = all_kitties_count.checked_add(1)
            .ok_or("Overflow adding a new Job to total supply")?;

        
        <JobOwner<T>>::insert(job_id, &to);
        <Jobs<T>>::insert(job_id, &new_job);
        <OwnedJob<T>>::insert(&to, &new_job); 
        <AllJobsArray<T>>::insert(all_kitties_count, job_id);
        <AllJobsCount<T>>::put(new_all_kitties_count);
        <AllJobsIndex<T>>::insert(job_id, all_kitties_count);
        <OwnedJobsArray<T>>::insert((to.clone(), owned_job_count), job_id);
        <OwnedJobsCount<T>>::insert(&to, new_owned_job_count);
        <OwnedJobsIndex<T>>::insert(job_id, owned_job_count);

        Self::deposit_event(RawEvent::Created(to, job_id));

        Ok(())
    }
    //used for transfering and buying/selling tokens
    fn transfer_from(from: T::AccountId, to: T::AccountId, job_id: T::Hash) -> Result {
        let owner = Self::owner_of(job_id).ok_or("No owner for this Job _ EventError")?;

        ensure!(owner == from, "'from' account does not own this Job");

        let owned_job_count_from = Self::owned_job_count(&from);
        let owned_job_count_to = Self::owned_job_count(&to);

        let new_owned_job_count_to = owned_job_count_to.checked_add(1)
            .ok_or("Transfer causes overflow of 'to' Job balance")?;

        let new_owned_job_count_from = owned_job_count_from.checked_sub(1)
            .ok_or("Transfer causes underflow of 'from' Job balance")?;

        let job_index = <OwnedJobsIndex<T>>::get(job_id);
        if job_index != new_owned_job_count_from {
            let last_job_id = <OwnedJobsArray<T>>::get((from.clone(), new_owned_job_count_from));
            <OwnedJobsArray<T>>::insert((from.clone(), job_index), last_job_id);
            <OwnedJobsIndex<T>>::insert(last_job_id, job_index);
        }

        <JobOwner<T>>::insert(&job_id, &to);
        <OwnedJobsIndex<T>>::insert(job_id, owned_job_count_to);
        <OwnedJobsArray<T>>::remove((from.clone(), new_owned_job_count_from));
        <OwnedJobsArray<T>>::insert((to.clone(), owned_job_count_to), job_id);
        <OwnedJobsCount<T>>::insert(&from, new_owned_job_count_from);
        <OwnedJobsCount<T>>::insert(&to, new_owned_job_count_to);

        Self::deposit_event(RawEvent::Transferred(from, to, job_id));

        Ok(())
    }
}

