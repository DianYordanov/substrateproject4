const inquirer = require('inquirer');
var exec = require('child_process').exec, child;

inquirer
  .prompt([
    {
      type: 'list',
      name: 'option',
      message: 'Choose an option:',
      choices: ['Make a commit', 'Visualise git', 'Go to origin', 'Show infographic', 'Show ohshitgit.com'],
    },
  ])
  .then(answers => {
    if (answers.option == "Make a commit"){
        const args = require('minimist')(process.argv.slice(2));

        if (process.argv.length==2){
            var arg1var;

            var questions1 = [{
                type: 'input',
                name: 'arg1',
                message: "Input argument 1:",
            }]
    
            inquirer.prompt(questions1).then(answers => {
                arg1var= (`${answers['arg1']}`);
    
                child = exec('git add . && git add -A  && git commit -m"' + arg1var + '" && git push',
                function (error, stdout, stderr) {
                    console.log('stdout: ' + stdout);
                    console.log('stderr: ' + stderr);
                    if (error !== null) {
                    console.log('exec error: ' + error);
                    
                }
            });
            })
        }
        if (process.argv.length>2){
            var ArgumentStringTwo = JSON.stringify(args).slice(6,JSON.stringify(args).length-2);
            var ArgumentStringThree = ArgumentStringTwo.split("\",\"").join(' ').slice(1,ArgumentStringTwo.split("\",\"").join(' ').length-1);;
            console.log('ArgumentStringThree: ' + ArgumentStringThree);
            child = exec('git add . && git add -A  && git commit -m"' + ArgumentStringThree + '" && git push',
            function (error, stdout, stderr) {
                console.log('stdout: ' + stdout);
                console.log('stderr: ' + stderr);
                if (error !== null) {
                    console.log('exec error: ' + error);
                    
                }
            });
        }
    }
    if (answers.option == "Visualise git"){
        child = exec('git log --graph',
        function (error, stdout, stderr) {
            console.log('stdout: ' + stdout);
            console.log('stderr: ' + stderr);
            if (error !== null) {
                console.log('exec error: ' + error);
            }
        });
    }
    if (answers.option == "Go to origin"){
        child = exec('git config --get remote.origin.url',
        function (error, stdout, stderr) {
            console.log('stdout: ' + stdout);
            console.log('stderr: ' + stderr);
            if (error !== null) {
                console.log('exec error: ' + error);
            }
            var opsys = process.platform;
            if (opsys == "darwin") {
                opsys = "MacOS";
            } else if (opsys == "win32" || opsys == "win64") {
                opsys = "Windows";
                var readableURL = MakeGitURLtoNormalURL(stdout);
                child = exec('start ' + readableURL,
                function (error, stdout, stderr) {
                    console.log('stdout: ' + stdout);
                    console.log('stderr: ' + stderr);
                    if (error !== null) {
                        console.log('exec error: ' + error);
                    }
                });
            } else if (opsys == "linux") {
                var readableURL = MakeGitURLtoNormalURL(stdout);
                child = exec('firefox ' + readableURL,
                function (error, stdout, stderr) {
                    console.log('stdout: ' + stdout);
                    console.log('stderr: ' + stderr);
                    if (error !== null) {
                        console.log('exec error: ' + error);
                    }
                });
            }
            console.log(opsys) // I don't know what linux is.
        });
    }
    if (answers.option == "Show infographic"){
        var opsys = process.platform;
        if (opsys == "darwin") {
            opsys = "MacOS";
        } else if (opsys == "win32" || opsys == "win64") {
            opsys = "Windows";
            child = exec('start http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf',
            function (error, stdout, stderr) {
                console.log('stdout: ' + stdout);
                console.log('stderr: ' + stderr);
                if (error !== null) {
                    console.log('exec error: ' + error);
                }
            });
        } else if (opsys == "linux") {
            opsys = "Linux";
            child = exec('firefox http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf',
            function (error, stdout, stderr) {
                console.log('stdout: ' + stdout);
                console.log('stderr: ' + stderr);
                if (error !== null) {
                    console.log('exec error: ' + error);
                }
            });
        }
        console.log(opsys) // I don't know what linux is.
    }
    if (answers.option == "Show ohshitgit.com"){
        var opsys = process.platform;
        if (opsys == "darwin") {
            opsys = "MacOS";
        } else if (opsys == "win32" || opsys == "win64") {
            opsys = "Windows";
            child = exec('start https://ohshitgit.com/',
            function (error, stdout, stderr) {
                console.log('stdout: ' + stdout);
                console.log('stderr: ' + stderr);
                if (error !== null) {
                    console.log('exec error: ' + error);
                }
            });
        } else if (opsys == "linux") {
            opsys = "Linux";
            child = exec('firefox https://ohshitgit.com/',
            function (error, stdout, stderr) {
                console.log('stdout: ' + stdout);
                console.log('stderr: ' + stderr);
                if (error !== null) {
                    console.log('exec error: ' + error);
                }
            });
        }
        console.log(opsys) // I don't know what linux is.
    }
// child();
});

function  MakeGitURLtoNormalURL(GitURl){

    if (GitURl.includes("gitlab.com")){
        var StringToReturn2 = GitURl.replace(/:/g, "/");
        return(StringToReturn2);
    }   
    if (GitURl.includes("bitbucket.org")){
        var pattern = /.*https:\/\/(.*)\@.*/gi;

        var StringToReturn1 = pattern.exec(GitURl);
        var StringToReturn2 = GitURl.replace(StringToReturn1[1], "").replace("@", "");
        return(StringToReturn2); 
    }
}


